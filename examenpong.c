/*******************************************************************************************
*
*   raylib game: FINAL PONG - game template
*
*   developed by [STUDENT NAME HERE]
*
*   This example has been created using raylib 1.0 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2014 Ramon Santamaria (Ray San)
*
********************************************************************************************/

#include "raylib.h"

typedef enum GameScreen { LOGO, TITLE, GAMEPLAY, ENDING } GameScreen;

int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
    int screenWidth = 800;
    int screenHeight = 450;
    char windowTitle[30] = "raylib game - FINAL PONG";
    
    GameScreen screen = LOGO;
    
    // TODO: Define required variables here..........................(0.5p)
    // NOTE: Here there are some useful variables (should be initialized)
    Rectangle player;
    player.width = 30;
    player.height = 80;
    player.x = 30;
    player.y = screenHeight/2 - player.height/2;
    int playerSpeedY = 10;
    
    Rectangle enemy;
    enemy.width = 30;
    enemy.height = 80;
    enemy.x = screenWidth - enemy.width - 30;
    enemy.y = screenHeight/2 - enemy.height/2;
    int enemySpeedY = 5;
    
    Rectangle neutral;
    neutral.width = 20;
    neutral.height = 40;
    neutral.x = screenWidth/2 - neutral.width/2 ;
    neutral.y = (screenHeight/2 - neutral.height/2) -112.5 ;
    
    Rectangle neutral2;
    neutral2.width = 20;
    neutral2.height = 40;
    neutral2.x = screenWidth/2 - neutral2.width/2 ;
    neutral2.y = (screenHeight/2 - neutral2.height/2) +112.5;
    
    
    
    Vector2 ballPosition;
    ballPosition.x = screenWidth/2;
    ballPosition.y = screenHeight/2;
    Vector2 ballSpeed;
    ballSpeed.x = 10;
    ballSpeed.y = 10;
    int ballRadius = 20;
    
    int topSpace = 50;
    
    Rectangle playerLife;
    playerLife.x = 10;
    playerLife.y = 10;
    playerLife.width = 200;
    playerLife.height = 30;
    
    Rectangle playerLifeBg = {5, 5, 210, 40};
    
    Rectangle enemyLife;
    enemyLife.width = 200;
    enemyLife.height = 30;
    enemyLife.x = screenWidth - enemyLife.width - 10;
    enemyLife.y = 10;
    
    Rectangle enemyLifeBg = {screenWidth - 210 - 5, 5, 210, 40};
    
    int damage = 20;
    
    bool pause = false;
    
    int secondsCounter = 99;
    
    int framesCounter;          // General pourpose frames counter
    
    int gameResult = -1;        // 0 - Loose, 1 - Win, -1 - Not defined
    
    InitWindow(screenWidth, screenHeight, windowTitle);
    
    Texture2D logo= LoadTexture("logo.png");
    Color logoColor = WHITE;
    logoColor = WHITE;
    logoColor.a = 0;
    
    Texture2D principio= LoadTexture("principio.png");
    Color principioColor = WHITE;
    principioColor = WHITE;
    principioColor.a = 0;
    
    bool fadeIn = true;
    
    int titlePos = -(MeasureText("Final Pong", 50));
    int titleSpeed = 5;
    
    int pressStartCounter = 0;
    
    Texture2D gameplayBackground = LoadTexture("fondo.png");
    // NOTE: If using textures, declare Texture2D variables here (after InitWindow)
    // NOTE: If using SpriteFonts, declare SpriteFont variables here (after InitWindow)
    
    // NOTE: If using sound or music, InitAudioDevice() and load Sound variables here (after InitAudioDevice)
    InitAudioDevice();      // Initialize audio device

    Sound fxWav = LoadSound("coin.wav");         // Load WAV audio file
    Sound fxOgg = LoadSound("Imperial_March.ogg");      // Load OGG audio file
    SetTargetFPS(60);
    //--------------------------------------------------------------------------------------
    
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------
        switch(screen) 
        {
            case LOGO: 
            {
                // Update LOGO screen data here!
                
                // TODO: Logo fadeIn and fadeOut logic...............(0.5p)
              if(fadeIn) 
              {
                  if(logoColor.a < 255)
                  {
                      logoColor.a++;
                  }
                  else
                  {
                     framesCounter++;
                     if(framesCounter >= 120){
                         fadeIn =false;
                     }
                  }
              } 
            else{
                if (logoColor.a > 0)
                {
                    logoColor.a--; 
                }  
              else  
              {
                  screen = TITLE;
                  framesCounter = 0;
              }                  
            }              
            } break;
            case TITLE: 
            {
                // Update TITLE screen data here!
                
                // TODO: Title animation logic.......................(0.5p)
                if (titlePos < screenWidth/2 - MeasureText("Final Pong", 50)/2)
                {
                    titlePos += titleSpeed;
                }
                else
                {
                    titlePos = screenWidth/2 - MeasureText("Final Pong", 50)/2;
                    framesCounter++;
                    if(framesCounter >=60) 
                    {
                        pressStartCounter++;
                        framesCounter = 0;
                    }
                }
                // TODO: "PRESS ENTER" logic.........................(0.5p)
                if (IsKeyPressed(KEY_ENTER))
                {
                   framesCounter = 0;
                   screen = GAMEPLAY;
                   PlaySound(fxOgg);      // Play OGG sound
                }
            } break;
            case GAMEPLAY:
            { 
                // Update GAMEPLAY screen data here!
                if (!pause)
                {
                    
                
                // TODO: Ball movement logic.........................(0.2p)
                ballPosition.x += ballSpeed.x;
                ballPosition.y += ballSpeed.y;
                // TODO: Player movement logic.......................(0.2p)
                if (IsKeyDown(KEY_UP)){
                    player.y -= playerSpeedY;
                }
                
                if (IsKeyDown(KEY_DOWN))
                {
                    player.y += playerSpeedY;
                }
                
                if(player.y <= topSpace)
                {
                    player.y = topSpace;
                }
                
                if (player.y + player.height >= screenHeight)
                {
                    player.y = screenHeight - player.height;
                }
                
                // TODO: Enemy movement logic (IA)...................(1p)
                if (ballPosition.x > screenWidth/2)
                {
                    if (ballPosition.y < enemy.y + enemy.height/2)
                        {
                         enemy.y -= enemySpeedY;   
                        }
                        else if (ballPosition.y > enemy.y + enemy.height/2)
                            {
                                enemy.y += enemySpeedY;
                            }
                }
                
                 if(enemy.y <= topSpace)
                {
                    enemy.y = topSpace;
                }
                
                if (enemy.y + enemy.height >= screenHeight)
                {
                    enemy.y = screenHeight - enemy.height;
                }
              
              
               
                // TODO: Collision detection (ball-player) logic.....(0.5p)
                if (CheckCollisionCircleRec(ballPosition, ballRadius, player))
                {
                    ballSpeed.x *= -1;
                    ballPosition.x = player.x + player.width + ballRadius; 
                     PlaySound(fxWav);
                }
                // TODO: Collision detection (ball-enemy) logic......(0.5p)
                if (CheckCollisionCircleRec(ballPosition, ballRadius, enemy))
                {
                    ballSpeed.x *= -1;
                    ballPosition.x = enemy.x - ballRadius; 
                    PlaySound(fxWav);
                }
                if(CheckCollisionCircleRec(ballPosition, ballRadius, neutral))
                {
                    ballSpeed.x *= -1;
                     PlaySound(fxWav);
                }
                
                 if(CheckCollisionCircleRec(ballPosition, ballRadius, neutral2))
                {
                    ballSpeed.x *= -1;
                    PlaySound(fxWav);
                }
                
                
                // TODO: Collision detection (ball-limits) logic.....(1p)
                if (ballPosition.y + ballRadius > screenHeight || ballPosition.y - ballRadius <= topSpace)
                {
                    ballSpeed.y *= -1;
                     PlaySound(fxWav);
                }
                
                
                // TODO: Life bars decrease logic....................(1p)
                if (ballPosition.x + ballRadius >= screenWidth)
                {
                    ballSpeed.x *= -1;
                    ballSpeed.x += 0.3;
                    enemyLife.width -= damage;
                    enemyLife.x += damage;
                    
                    ballPosition.x = screenWidth/2;
                    ballPosition.y = screenHeight/2; 
                }
                }
                if(ballPosition.x - ballRadius <=0)
                {
                    ballSpeed.x *= -1;
                    playerLife.width -= damage;
                    
                    ballPosition.x = screenWidth/2;
                    ballPosition.y = screenHeight/2;
                }
                // TODO: Time counter logic..........................(0.2p)
                  framesCounter++;
                  if (framesCounter > 60)
                      {
                       secondsCounter--;
                       framesCounter = 0;
                       // TODO: Game ending logic...........................(0.2p)
                       if (playerLife.width <= 0)
                       {
                           screen = ENDING;
                           gameResult = 0;
                       }
                       
                       if(enemyLife.width <= 0)
                       {
                           screen = ENDING;
                           gameResult = 1;
                       }
                        if(secondsCounter <= 0)
                       {
                           screen = ENDING;
                           gameResult = 1;
                       }
                           
                      }
                
                
                // TODO: Pause button logic..........................(0.2p)
                if (IsKeyPressed(KEY_ENTER))
                {
                    pause = !pause;
                }
            } break;
            case ENDING: 
            {
                // Update END screen data here!
                
                // TODO: Replay / Exit game logic....................(0.5p)
                
                if(IsKeyPressed(KEY_ENTER))
                {
                    playerLife.width = 200;
                    enemyLife.width = 200;
                    enemyLife.x = screenWidth - enemyLife.width - 10;   
                    secondsCounter = 99;
                    framesCounter = 0;
                    ballPosition.x = screenWidth/2;
                    ballPosition.y = screenHeight/2;
                    
                    screen = GAMEPLAY;
                }
            } break;
            default: break;
        }
        //----------------------------------------------------------------------------------
        
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();
        
            ClearBackground(RAYWHITE);
            
            switch(screen) 
            {
                case LOGO: 
                {
                    // Draw LOGO screen here!
                    
                    // TODO: Draw Logo...............................(0.2p)
                    
                 ClearBackground(RAYWHITE);

            DrawTexture(logo, screenWidth/2 - logo.width/2, screenHeight/2 - logo.height/2, logoColor);
               
               
                } break;
                case TITLE: 
                {
                    // Draw TITLE screen here!
                    DrawTexture(principio, 0, 0, WHITE);
                    // TODO: Draw Title..............................(0.2p)
                    
                    DrawText("Final Pong", titlePos, screenHeight/4, 50, RED);
                    // TODO: Draw "PRESS ENTER" message..............(0.2p)
                    if(pressStartCounter % 2 != 0) DrawText("Press Start", screenWidth/2 - MeasureText("Press Start", 30)/2, screenHeight/3 * 2, 30, RED);
                    
                    
                    
                } break;
                case GAMEPLAY:
                { 
                    // Draw GAMEPLAY screen here!
                    DrawTexture(gameplayBackground, 0, 0, WHITE);
                    DrawCircleV(ballPosition, ballRadius, PINK);
                    // TODO: Draw player and enemy...................(0.2p)
                    DrawRectangleRec(player, BLUE); 
                    DrawRectangleRec(enemy, RED); 
                    DrawRectangleRec(neutral, YELLOW);
                    DrawRectangleRec(neutral2, YELLOW);
                    // TODO: Draw player and enemy life bars.........(0.5p)
                    DrawRectangleRec(playerLifeBg, BLACK);
                    DrawRectangleRec(playerLife, BLUE);
                    
                    DrawRectangleRec(enemyLifeBg, BLACK);
                    DrawRectangleRec(enemyLife, RED);
                    // TODO: Draw time counter.......................(0.5p)
                    DrawText(FormatText("%i", secondsCounter), screenWidth/2 - MeasureText(FormatText("%i", secondsCounter), 40)/2, 10, 40, BLACK);
                    // TODO: Draw pause message when required........(0.5p)
                    if (pause)
                    {
                        DrawText("PAUSE", screenWidth/2- MeasureText("PAUSE", 50)/2, screenHeight/2, 50, GRAY);
                    }
                } break;
                case ENDING: 
                {
                    // Draw END screen here!
                    DrawTexture(gameplayBackground, 0, 0, WHITE);
                    // TODO: Draw ending message (win or loose)......(0.2p)
                    if (gameResult ==1)
                    {
                         DrawText("YOU WIN", screenWidth/2- MeasureText("YOU WIN", 50)/2, screenHeight/3, 50, GRAY);
                    }
                    else if (gameResult == 0)
                    {
                        DrawText("YOU LOOSE", screenWidth/2- MeasureText("YOU LOOSE", 50)/2, screenHeight/3, 50, GRAY);
                        
                    }
                    else
                    {
                     DrawText("DRAW GAME", screenWidth/2- MeasureText("DRAW GAME", 50)/2, screenHeight/3, 50, GRAY);   
                    }
                    DrawText ("Press Enter to restart",screenWidth/2- MeasureText("DRAW GAME", 50)/2, screenHeight/3 * 2, 30, BLACK);
                } break;
                default: break;
            }
        
            DrawFPS(10, 10);
        
        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    UnloadTexture(logo);
    UnloadTexture(gameplayBackground);
    UnloadTexture(principio);
    UnloadSound(fxWav); 
    UnloadSound(fxOgg);
    
    // NOTE: Unload any Texture2D or SpriteFont loaded here
    
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------
    
    return 0;
}